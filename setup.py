from setuptools import setup, find_packages
from src.setting import PATH_TRASH
from os.path import join

setup(name='trash',
      version='1.0',
      description='Recycle Bin Utility',
      author='Kozlov Andrew',
      author_email='fromligthbeam@gmail.com',
      packages=find_packages(),
      data_files=[
        (PATH_TRASH, [".config"]),
        (join(PATH_TRASH, 'files'), []),
        (join(PATH_TRASH, 'info'), []),
      ],
      entry_points={
            'console_scripts': [
            'trash = src.rm:main'
      ]
      })
