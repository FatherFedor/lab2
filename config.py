import json


def write_info(path_info, info, format, flags):
    if not flags.get('dry_run'):
        if format == 'json':
            with open(path_info, 'w') as info_file:
                json.dump(info, info_file, indent=4, separators=(',', ': '))


def read_info(path_info_file):
    with open(path_info_file) as info_file:
        return json.load(info_file)


def initial_config(path_config = '/home/light/PyLabs/Lab_0002/.config'):
    with open(path_config) as f:
        # print f.read()
        setting = json.load(f)
    return setting