import argparse


def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument('-v', '--verbose', action='store_true', help='detailed report after each operation')
    parser.add_argument('-s', '--silent', action='store_true', help='return only code')
    parser.add_argument('-d', '--dryrun', action='store_true', help='only write about changes, not perform him')
    parser.add_argument('-f', '--force', action='store_true', help='do not ask anything')
    parser.add_argument('-i', '--interactive', action='store_true', help='ask confirm each operations')

    parser.add_argument('--config', action='store', help='you can transfer own config file')

    subparsers = parser.add_subparsers(title='subcommands',
                                       description='valid subcommands',
                                       help='additional help',
                                       dest='command')

    parser_delete = subparsers.add_parser('delete', help='delete file or directory')
    parser_delete.add_argument('file', type=str, help='file or directory for deleting')
    parser_delete.add_argument('--regex', action='store', help='deleting object by regular expression')

    parser_restore = subparsers.add_parser('restore', help='restore file in directory when it was deleting')
    parser_restore.add_argument('file', type=str, help='file for restoring')

    subparsers.add_parser('show', help='show list of trash')

    subparsers.add_parser('clean', help='clean all trash irretrievably')

    # parser_clear.add_argument('file', type=str, help='file for clear')

    return parser.parse_args()
