import datetime
import logging
import os
import re
import shutil

import config

from politics import PoliticConfilct, PoliticDelete, PoliticRestore


class Trash(object):
    def __init__(self, policy_delete='replace', policy_restore='qen_uniq'):
        self.delete_obj = getattr(PoliticDelete, 'delete_' + policy_delete)
        self.restore_obj = getattr(PoliticRestore, 'restore_' + policy_restore)

    def clean_trash(self, *paths, **flags):
        if not flags.get('dry_run'):
            for p in paths:
                shutil.rmtree(p)
                os.mkdir(p)
                logging.info('trash is clear')
        else:
            for path in paths:
                print '%s this files in this directory will delete',
                for f in os.listdir(path):
                    print '%-12s' % os.path.basename(f),
                print '\n'

    def restore(self, path, path_directory_of_trash, path_info, policy='qen_uniq', **flags):
        trash_path = os.path.join(path_directory_of_trash, path)
        if not os.path.exists(trash_path):
            logging.error('%s doesn\'t exist' % path)
            raise OSError('%s doesn\'t exist' % path)
        path_info_file = os.path.join(path_info, os.path.basename(path))
        self.restore_obj()

    def get_size(self, start_path):
        total_size = 0
        for dirpath, dirnames, filenames in os.walk(start_path):
            for f in filenames:
                fp = os.path.join(dirpath, f)
                total_size += os.path.getsize(fp)
        return total_size

    def check_size(self, path, path_directory_of_trash, max_size):
        trash_size = self.get_size(path_directory_of_trash)
        delete_size = self.get_size(path)
        if trash_size + delete_size > max_size:
            return False
        else:
            return True

    def iscycle(self, path, path_directory_of_trash):
        if path.find(path_directory_of_trash) == 0:
            return True
        else:
            return False

    def check_error(self, path, path_directory_of_trash, max_size):
        if not self.check_size(path, path_directory_of_trash, max_size):
            #poltics politics politics
            logging.error('not enough size')
            raise OSError('not enough size')
        if self.iscycle(path, path_directory_of_trash):
            raise OSError('you try delete in trash directory')
        if not os.path.exists(path):
            logging.info('%s   doesn\'t exist' % path)
            raise OSError('%s   doesn\'t exist' % path)

    def delete(self, path, path_directory_of_trash, path_info, policy='replace', max_size=500, **flags):
        path = os.path.abspath(path)
        self.check_error(path, path_directory_of_trash, max_size)

        trash_path = os.path.join(path_directory_of_trash, os.path.basename(path))
        path_info = os.path.join(path_info, os.path.basename(trash_path))
        self.delete_obj(path, trash_path, path_info, flags)

    def find_regex(self, regex, path):
        res = []
        for dirpath, dirnames, filenames in os.walk(path):
            for filename in filenames:
                if re.match(regex, os.path.basename(filename)):
                    res.append(os.path.join(dirpath, filename))
            for dirname in dirnames:

                if re.match(regex, os.path.basename(dirname)):
                    res.append(os.path.join(dirpath, dirname))
        return res

    def regex_delete(self, regex, path, path_directory_of_trash, path_info, max_size=500, **flags):
        for path in reversed(self.find_regex(regex, path)):
            self.delete(path,
                        path_directory_of_trash,
                        path_info,
                        dry_run=flags.get('dry_run'),
                        verbose=flags.get('verbose'),
                        silent=flags.get('silent'),
                        interactive=flags.get('interactive'),
                        force=flags.get('force'))

    def show(self, path):
        for f in os.listdir(path):
            print '%-12s' % f,
        logging.info('show directory')

    def auto_clean_trash(self, path_directory_of_trash, path_info, politic_setting):
        for name_info in os.listdir(path_info):
            info = config.read_info(os.path.join(path_info, name_info))
            if politic_setting['politic'] == 'time':
                min_valid_date = str(datetime.datetime.now() - datetime.timedelta(minutes=int(politic_setting['value'])) )
                #noonono
                if info['date_delete'] < min_valid_date:
                    self.remove_from_trash(os.path.join(path_directory_of_trash, os.path.basename(name_info)))
                    os.remove(os.path.join(path_info, name_info))
                    logging.info('clean: {0} policy: {1} value: {2}'.format(name_info, politic_setting['politic'], politic_setting['value']))

    def remove_from_trash(self, path):
        if os.path.isfile(path):
            os.remove(path)
        if os.path.isdir(path):
            shutil.rmtree(path)
