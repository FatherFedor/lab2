import datetime
import logging
import os

from asker import ask

import config


class PoliticConfilct(object):
    @classmethod
    def generate_unique_name(cls, trash_path):
        i = 1
        name, ext = os.path.splitext(trash_path)
        non_exists_path = name + '__%05d' + ext
        while os.path.exists(non_exists_path % i):
            i += 1
        return non_exists_path % i


class PoliticDelete(object):
    @classmethod
    def delete_recursive(cls, path, trash_path, flags):
        if os.path.isfile(path):
            if ask(path, flags):
                if not flags.get('dry_run'):
                    os.rename(path, trash_path)
        elif os.path.isdir(path):
            if ask(path, flags):
                os.makedirs(trash_path)
                for p in os.listdir(path):
                    cls.delete_recursive(os.path.join(path, p),
                                         os.path.join(trash_path, p), flags)
                if os.listdir(path) == []:
                    os.rmdir(path)

    @classmethod
    def delete_gen_uniq(cls, path, trash_path, path_info, flags):
        if os.path.exists(trash_path):
            trash_path = PoliticConfilct.generate_unique_name(trash_path)
            cls.delete_recursive(path, trash_path, flags)
            config.write_info(path_info, {'path': path, 'date_delete': str(datetime.datetime.now())}, 'json', flags)
            logging.info('{0} - successfull was removed\n {1} - file in trash directory\n {2} flags\n '.format(path, trash_path, flags))
            return 201
        else:
            cls.delete_recursive(path, trash_path, flags)
            config.write_info(path_info, {'path': path, 'date_delete': str(datetime.datetime.now())}, 'json', flags)
            return 200

    @classmethod
    def delete_replace(cls, path, trash_path, path_info, flags):
        if os.path.exists(trash_path):
            cls.delete_recursive(path, trash_path, flags)
            config.write_info(path_info, {'path': path, 'date_delete': str(datetime.datetime.now())}, 'json', flags)
            logging.info('%s  - successfull was removed' % path)
            return 202
        else:
            cls.delete_recursive(path, trash_path, flags)
            config.write_info(path_info, {'path': path, 'date_delete': str(datetime.datetime.now())}, 'json', flags)
            return 200

    @classmethod
    def delete_cancel(cls, path, trash_path, path_info, flags):
        if os.path.exists(trash_path):
            logging.info('cancel deleting %s' % path)
            return 401
        else:
            cls.delete_recursive(path, trash_path, flags)
            config.write_info(path_info, {'path': path, 'date_delete': str(datetime.datetime.now()) }, 'json', flags)
            logging.info('%s  -  successfull was removed' % path)
            return 200


class PoliticRestore(object):
    @classmethod 
    def restore_rename(cls, trash_path, restore_path, path_info_file)
        if not flags.get('dry_run'):
            os.rename(trash_path, restore_path)
            os.remove(path_info_file)

    @classmethod
    def restore_qen_uniq(cls, trash_path, path_info_file):
        info = config.read_info(path_info_file)
        restore_path = info['path']

        if os.path.exists(restore_path):
            restore_path = PoliticConfilct.generate_unique_name(restore_path)
            restore_rename(trash_path, restore_path, path_info_file)
        else:
            restore_rename(trash_path, restore_path, path_info_file)

    @classmethod
    def restore_replace(cls, path, path_directory_of_trash):
        info = config.read_info(path_info_file)
        restore_path = info['path']
        restore_rename(trash_path, restore_path, path_info_file)

    @classmethod
    def restore_cacel(cls, path, path_directory_of_trash):
        info = config.read_info(path_info_file)
        restore_path = info['path']

        if os.path.exists(restore_path):
            logging.error('%s file with this name already exists' % restore_path)
            raise OSError('%s file with this name already exists' % restore_path)
        else:
            restore_rename(trash_path, restore_path, path_info_file)
