import os

PATH_TRASH = os.path.expanduser('~/trash')

PATH_CONFIG = os.path.join(PATH_TRASH, '.config')

DEF_CONFIG = {
    'path_trash': os.path.join(PATH_TRASH, 'files'),
    'path_info_files': os.path.join(PATH_TRASH, 'info'),
    'path_log': os.path.join(PATH_TRASH, 'trash.log'),
    'policy_auto_deleting': 'time',
    'policy_conflict_deleting': 'replace',
    'policy_conflict_restore': 'replace',
    'maxsize': 1024
}
