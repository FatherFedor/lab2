import os


def ask_y_n(str):
    answer = raw_input(str)
    if answer == 'y':
        return True
    elif answer == 'n':
        return False


def ask(path, flags):
    if flags.get('force'):
        return True

    if flags.get('interactive') or not os.access(path, os.W_OK):
        if os.path.isfile(path):
            return ask_y_n('Do you want delete this file %s?: ' % path)
        elif os.path.isdir(path):
            return ask_y_n('Do you want come down this directory %s: ' % path)
    return True
