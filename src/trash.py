
import logging
import os
import re
import sys
import shutil

from checker import CheckDelete
from helper import get_size
from politics import PoliticAutoDelete, PoliticDelete, PoliticRestore
from log import send_message


class Trash(object):
    def __init__(self, policy_delete='replace', policy_restore='replace', policy_clear='time', **flags):
        self.delete_obj = PoliticDelete.get_policy_delete(policy_delete)
        self.restore_obj = PoliticRestore.get_policy_restore(policy_restore)
        self.clear_trash = PoliticAutoDelete.get_policy_auto_delete(policy_clear)
        if flags.get('silent'):
            self.silent = flags.get('silent')
        if flags.get('path_log'):
            self.path_log = flags.get('path_log')

    def clear_all_trash(self, *paths, **flags):
        """Clear all trash.

        Remove folder and make new empty.
        """
        if not flags.get('dry_run'):
            for p in paths:
                shutil.rmtree(p)
                os.mkdir(p)
        else:
            for path in paths:
                print '%s this files in this directory will delete',
                for f in os.listdir(path):
                    print '%-12s' % os.path.basename(f),

    def restore(self, path, path_directory_of_trash, path_info, **flags):
        trash_path = os.path.join(path_directory_of_trash, path)
        if not os.path.exists(trash_path):
            send_message('%s doesn\'t exist' % trash_path, self.path_log, self.silent)
            sys.exit(1)
        path_info_file = os.path.join(path_info, os.path.basename(path))
        self.restore_obj(trash_path, path_info_file, flags)
        send_message('%s restored' % path, self.path_log, self.silent)


    def delete(self, path, path_directory_of_trash, path_info, max_size=500, **flags):
        path = os.path.abspath(path)
        if os.path.exists(path):
            send_message('%s doesn\'t exist' % path, self.path_log, self.silent)
            sys.exit(1)
        need_space = CheckDelete.check_error(path, path_directory_of_trash, path_info, max_size)
        if need_space:
            self.auto_clear_trash(path_directory_of_trash, path_info, need_space)
        trash_path = os.path.join(path_directory_of_trash, path)
        path_info = os.path.join(path_info, os.path.basename(trash_path))
        self.delete_obj(path, trash_path, path_info, flags)
        send_message('%s deleted' % path, self.path_log, self.silent)

    def find_regex(self, regex, path):
        res = []
        for dirpath, dirnames, filenames in os.walk(path):
            for filename in filenames:
                if re.match(regex, os.path.basename(filename)):
                    res.append(os.path.join(dirpath, filename))
            # for dirname in dirnames:
            #     if re.match(regex, os.path.basename(dirname)):
            #         res.append(os.path.join(dirpath, dirname))
        return res

    def regex_delete(self, regex, path, path_directory_of_trash, path_info, max_size=500, **flags):
        for path in reversed(self.find_regex(regex, path)):
            self.delete(path,
                        path_directory_of_trash,
                        path_info,
                        dry_run=flags.get('dry_run'),
                        interactive=flags.get('interactive'),
                        force=flags.get('force'))

    def show(self, path):
        for f in os.listdir(path):
            print '%-12s' % f,
        # self.logger.info('show directory')

    def auto_clear_trash(self, path_directory_of_trash, path_directory_info, need_space):
        print self.__dict__
        list_info_for_delete = self.clear_trash(path_directory_info, need_space)
        for info in list_info_for_delete:
            path_info = os.path.join(path_directory_info, info)
            path_trash = os.path.join(path_directory_of_trash, info)
            self.remove_from_trash(path_trash, path_info)

    def remove_from_trash(self, path, path_info):
        if os.path.isfile(path):
            os.remove(path)
        if os.path.isdir(path):
            shutil.rmtree(path)
        os.remove(path_info)
