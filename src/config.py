import json
import os

from setting import DEF_CONFIG, PATH_CONFIG


def write_default_config():
    with open(PATH_CONFIG, 'w') as f:
        json.dump(DEF_CONFIG, f, indent=4, separators=(',', ': '))


def write_info(path_info, info, format, flags):
    if not flags.get('dry_run'):
        if format == 'json':
            with open(path_info, 'w') as info_file:
                json.dump(info, info_file, indent=4, separators=(',', ': '))
        if format == 'ini':
            pass


def read_info(path_info_file):
    with open(path_info_file) as info_file:
        return json.load(info_file)


def update_by_args(args):
    for key, value in args.iteritems():
        if value is not None:
            DEF_CONFIG[key] = value


def initial_config(path_config, args):
    if os.path.exists(path_config):
        with open(path_config) as f:
            DEF_CONFIG.update(json.load(f))
    update_by_args(args)
    return DEF_CONFIG
