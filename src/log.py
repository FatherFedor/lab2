import logging


def send_message(cls, mess, path_log, silent):
    if path_log:
        logging.basicConfig(filemode='a',
                            filename='path_log',
                            level=logging.DEBUG,
                            datefmt='%m/%d/%Y %I:%M:%S %p')
        logging.info(mess)
    if not silent:
        print mess
