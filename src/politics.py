import datetime
import logging
import os
import sys

import config

from asker import ask
from helper import get_size
from log import send_message



class PoliticConfilct(object):
    @classmethod
    def generate_unique_name(cls, trash_path):
        i = 1
        name, ext = os.path.splitext(trash_path)
        non_exists_path = name + '__%05d' + ext
        while os.path.exists(non_exists_path % i):
            i += 1
        return non_exists_path % i


class PoliticDelete(object):
    @classmethod
    def delete_recursive(cls, path, trash_path, flags):
        if os.path.isfile(path):
            if ask(path, flags):
                if not flags.get('dry_run'):
                    os.rename(path, trash_path)
        elif os.path.isdir(path):
            if ask(path, flags):
                os.makedirs(trash_path)
                for p in os.listdir(path):
                    cls.delete_recursive(os.path.join(path, p),
                                         os.path.join(trash_path, p), flags)
                if os.listdir(path) == []:
                    os.rmdir(path)

    @classmethod
    def get_policy_delete(cls, name):
        if name == 'gen_uniq':
            return cls.delete_gen_uniq
        if name == 'cancel':
            return cls.delete_cancel
        if name == 'replace':
            return cls.delete_replace

    @classmethod
    def delete_gen_uniq(cls, path, trash_path, path_info, flags):
        if os.path.exists(trash_path):
            trash_path = PoliticConfilct.generate_unique_name(trash_path)
            cls.delete_recursive(path, trash_path, flags)
            config.write_info(path_info, {'path': path, 'date_delete': str(datetime.datetime.now())}, 'json', flags)

        else:
            cls.delete_recursive(path, trash_path, flags)
            config.write_info(path_info, {'path': path, 'date_delete': str(datetime.datetime.now())}, 'json', flags)

    @classmethod
    def delete_replace(cls, path, trash_path, path_info, flags):
        if os.path.exists(trash_path):
            cls.delete_recursive(path, trash_path, flags)
            config.write_info(path_info, {'path': path, 'date_delete': str(datetime.datetime.now())}, 'json', flags)

        else:
            cls.delete_recursive(path, trash_path, flags)
            config.write_info(path_info, {'path': path, 'date_delete': str(datetime.datetime.now())}, 'json', flags)

    @classmethod
    def delete_cancel(cls, path, trash_path, path_info, flags):
        if os.path.exists(trash_path):
            logging.info('cancel deleting %s' % path)
            sys.exit(1)
        else:
            cls.delete_recursive(path, trash_path, flags)
            config.write_info(path_info, {'path': path, 'date_delete': str(datetime.datetime.now()) }, 'json', flags)
            logging.info('%s  -  successfull was removed' % path)


class PoliticRestore(object):
    @classmethod 
    def restore_rename(cls, trash_path, restore_path, path_info_file, flags):
        if ask(path, flags):
            if not flags.get('dry_run'):
                os.rename(trash_path, restore_path)
                os.remove(path_info_file)

    @classmethod
    def get_policy_restore(cls, name):
        if name == 'gen_uniq':
            return cls.restore_qen_uniq
        if name == 'replace':
            return cls.restore_replace
        if name == 'cancel':
            return cls.restore_cancel

    @classmethod
    def restore_qen_uniq(cls, trash_path, path_info_file, flags):
        info = config.read_info(path_info_file)
        restore_path = info['path']

        if os.path.exists(restore_path):
            restore_path = PoliticConfilct.generate_unique_name(restore_path)
            cls.restore_rename(trash_path, restore_path, path_info_file, flags)
        else:
            cls.restore_rename(trash_path, restore_path, path_info_file, flags)

    @classmethod
    def restore_replace(cls, trash_path, path_info_file, flags):
        info = config.read_info(path_info_file)
        restore_path = info['path']
        cls.restore_rename(trash_path, restore_path, path_info_file, flags)

    @classmethod
    def restore_cancel(cls, trash_path, path_info_file, flags):
        info = config.read_info(path_info_file)
        restore_path = info['path']

        if os.path.exists(restore_path):
            send_message('%s file with this name already exists' % restore_path, 
                         flags.get('path_log'), 
                         flags.get('silent'))
            sys.exit(1)
        else:
            cls.restore_rename(trash_path, restore_path, path_info_file, flags)


class PoliticAutoDelete(object):
    @classmethod
    def get_info(cls, path_info):
        list_info = []
        for name_info in os.listdir(path_info):
            list_info.append(config.read_info(os.path.join(path_info, name_info)))
        return list_info

    @classmethod
    def get_policy_auto_delete(cls, name):
        if name == 'time':
            return cls.clear_by_time
        if name == 'size':
            return cls.clear_by_size

    @classmethod
    def clear_by_time(cls, path_info, need_space):
        file_for_del = []
        list_info = cls.get_info(path_info)
        list_info.sort(key=lambda info: info['date_delete'], reverse=True)
        space = 0
        for file in list_info:
            if space < need_space:
                file_for_del.append(file)
            else:
                break
        return file_for_del

    @classmethod
    def clear_by_size(cls, path_info, need_space):
        file_for_del = []
        list_info = cls.get_info(path_info)
        list_info.sort(key=lambda info: get_size(info), reverse=True)
        space = 0
        for file in list_info:
            if space < need_space:
                file_for_del.append(file)
            else:
                break
        return file_for_del
