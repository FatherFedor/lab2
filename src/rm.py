#! usr/bin/env python

import sys

from config import initial_config, write_default_config
from parsermarg import parse_args
from setting import PATH_CONFIG
from trash import Trash


def main():

    args = vars(parse_args())

    write_default_config()

    if args['config']:
        setting = initial_config(args['config'], args)
    else:
        setting = initial_config(PATH_CONFIG, args)
    with open(setting['path_log'], 'w'):
        pass

    t = Trash(setting['policy_conflict_deleting'],
              setting['policy_conflict_restore'],
              setting['policy_auto_deleting'],
              path_log=setting['path_log'],
              silent=args['silent'],
              )

    if args['command'] == 'show':
        t.show(setting['path_trash'])

    if args['command'] == 'delete':
        if args['regex']:
            t.regex_delete(args['regex'],
                           args['file'],
                           setting['path_trash'],
                           setting['path_info_files'],
                           dry_run=args['dryrun'],
                           interactive=args['interactive'],
                           force=args['force'])
        else:
            t.delete(args['file'],
                     setting['path_trash'],
                     setting['path_info_files'],
                     dry_run=args['dryrun'],
                     interactive=args['interactive'],
                     force=args['force'])

    if args['command'] == 'clean':
        t.clean_all_trash(setting['path_trash'])

    if args['command'] == 'restore':
        t.restore(args['file'],
                  setting['path_trash'],
                  setting['path_info_files'],
                  dry_run=args['dryrun'],
                  interactive=args['interactive'],
                  force=args['force'])
    sys.exit(0)

if __name__ == '__main__':
    main()
