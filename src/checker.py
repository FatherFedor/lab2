import logging
import os

from helper import get_size, make_dir


class CheckDelete(object):
    @classmethod
    def check_size(cls, path, path_directory_of_trash, max_size):
        trash_size = get_size(path_directory_of_trash)
        delete_size = get_size(path)
        if trash_size > max_size:
            logging.error('not enough size')
            raise OSError('not enough size')
        if trash_size + delete_size > max_size:
            return trash_size + delete_size - max_size
        else:
            return 0

    @classmethod
    def iscycle(cls, path, path_directory_of_trash):
        if path.find(path_directory_of_trash) == 0:
            return True
        else:
            return False

    @classmethod
    def check_error(cls, path, path_directory_of_trash, path_info, max_size):
        make_dir(path_directory_of_trash)
        make_dir(path_info)
        if cls.iscycle(path, path_directory_of_trash):
            raise OSError('you try delete in trash directory')
        if not os.path.exists(path):
            logging.error('%s   doesn\'t exist' % path)
            raise OSError('%s   doesn\'t exist' % path)
        return cls.check_size(path, path_directory_of_trash, max_size)
