import unittest
import os
import shutil
from trash import Trash

class RemoveTestCase(unittest.TestCase):
    def setUp(self):
        self.path = os.path.expanduser("~/PyLabs/Lab__00002/test_dir")
        self.trash_path = os.path.expanduser("~/PyLabs/Lab__00002/test_dir/trash/files")
        self.info_path = os.path.expanduser("~/PyLabs/Lab__00002/test_dir/trash/info")
        self.t = Trash()
        if not os.path.exists(self.path):
            os.makedirs(self.path)
        if not os.path.exists(os.path.expanduser("~/PyLabs/Lab__00002/test_dir/trash/")):
            os.makedirs(os.path.expanduser("~/PyLabs/Lab__00002/test_dir/trash/"))
        if not os.path.exists(self.trash_path):
            os.makedirs(self.trash_path)
        if not os.path.exists(self.info_path):
            os.makedirs(self.info_path)
   
    def test_normal_working_deleting(self):
        file_path = os.path.join(self.path, "test.txt")
        with open(file_path, "w"):
            pass
        self.t.delete(file_path, self.trash_path, self.info_path)
        self.assertFalse(os.path.exists(file_path))
        self.assertTrue(os.path.exists(os.path.join(self.trash_path, "test.txt")))

    def test_nofile_deleting(self):
        file_path = os.path.join(self.path, "test.txt")
        self.t.delete(file_path, self.trash_path, self.info_path)
        self.assertFalse(os.path.exists(os.path.join(self.trash_path, "file_Path")))

    def test_multiple_files_deleting(self):     
        file_path1 = os.path.join(self.path, "test.txt")
        with open(file_path1, "w"):
            pass
        file_path2 = os.path.join(self.path, "some.txt")
        with open(file_path2, "w"):
            pass
        file_path3 = os.path.join(self.path, "python.txt")
        with open(file_path3, "w"):
            pass
        self.t.delete(file_path1, self.trash_path, self.info_path)            
        self.t.delete(file_path2, self.trash_path, self.info_path)
        self.t.delete(file_path3, self.trash_path, self.info_path)

        self.assertFalse(os.path.exists(file_path1))
        self.assertFalse(os.path.exists(file_path2))
        self.assertFalse(os.path.exists(file_path3))

        self.assertTrue(os.path.exists(os.path.join(self.trash_path, "test.txt")))
        self.assertTrue(os.path.exists(os.path.join(self.trash_path,"some.txt")))
        self.assertTrue(os.path.exists(os.path.join(self.trash_path, "python.txt")))

    def test_regex_deleting(self):
        dir_path1 = os.path.join(self.path, "Dir")
        dir_path2 = os.path.join(dir_path1, "Dir1")
        dir_path3 = os.path.join(dir_path1, "Dir2")

        os.makedirs(dir_path1)
        os.makedirs(dir_path2)
        os.makedirs(dir_path3)

        file_path1 = os.path.join(dir_path1, "test.txt")
        with open(file_path1, 'w'):
            pass
        file_path2 = os.path.join(dir_path1, "some.txt")
        with open(file_path2, 'w'):
            pass
        file_path3 = os.path.join(dir_path1, "python.txt")
        with open(file_path3, 'w'):
            pass
        file_path4 = os.path.join(dir_path2, "test23.txt")
        with open(file_path4, 'w'):
            pass
        file_path5 = os.path.join(dir_path2, "some42342.txt")
        with open(file_path5, 'w'):
            pass
        file_path6 = os.path.join(dir_path2, "pyt543534hon.log")
        with open(file_path6, 'w'):
            pass
        file_path7 = os.path.join(dir_path3, "tes32.txt")
        with open(file_path7, 'w'):
            pass
        file_path8 = os.path.join(dir_path3, "test.log")
        with open(file_path8, 'w'):
            pass

        self.t.regex_delete('test', dir_path1, self.trash_path, self.info_path)
        
        self.assertFalse(os.path.exists(file_path1))
        self.assertFalse(os.path.exists(file_path4))
        self.assertFalse(os.path.exists(file_path8))
        
        self.assertTrue(os.path.exists(file_path2))
        self.assertTrue(os.path.exists(file_path3))
        self.assertTrue(os.path.exists(file_path5))
        self.assertTrue(os.path.exists(file_path6))
        self.assertTrue(os.path.exists(file_path7))

        self.assertFalse(os.path.exists(os.path.join(self.trash_path, "some.txt")))
        self.assertFalse(os.path.exists(os.path.join(self.trash_path, "python.txt")))
        self.assertFalse(os.path.exists(os.path.join(self.trash_path, "some42342.txt")))
        self.assertFalse(os.path.exists(os.path.join(self.trash_path, "pyt543534hon.log")))
        self.assertFalse(os.path.exists(os.path.join(self.trash_path, "tes32.txt")))

        self.assertTrue(os.path.exists(os.path.join(self.trash_path, "test.txt")))
        self.assertTrue(os.path.exists(os.path.join(self.trash_path, "test23.txt")))
        self.assertTrue(os.path.exists(os.path.join(self.trash_path, "test.log")))

    def test_conflict_generate_uniq_deleting(self):     
        file_path = os.path.join(self.path, "test.txt")
        with open(file_path, "w"):
            pass
        self.t.delete(file_path, self.trash_path, self.info_path, 'gen_uniq')
        with open(file_path, "w"):
            pass
        self.t.delete(file_path, self.trash_path, self.info_path, 'gen_uniq')
         
        self.assertFalse(os.path.exists(file_path))

        self.assertTrue(os.path.exists(os.path.join(self.trash_path, "test.txt")))
        self.assertTrue(os.path.exists(os.path.join(self.trash_path, "test__00001.txt")))

    def test_conflict_replace_deleting(self):
        file_path = os.path.join(self.path, "test.txt")
        with open(file_path, "w"):
            pass
        self.t.delete(file_path, self.trash_path, self.info_path, 'replace')
        with open(file_path, "w") as file:
            file.write("new file")
        self.t.delete(file_path, self.trash_path, self.info_path, 'replace')
        
        self.assertFalse(os.path.exists(file_path))
        self.assertFalse(os.path.exists(os.path.join(self.trash_path, "test__00001.txt")))

        self.assertTrue(os.path.exists(os.path.join(self.trash_path, "test.txt")))

        with open(os.path.join(self.trash_path, "test.txt")) as file:
            self.assertEqual(file.readline(), "new file")

    def test_conflict_cancel_deleting(self):      
        file_path = os.path.join(self.path, "test.txt")
        with open(file_path, "w") as file:
            file.write("first file")
        self.t.delete(file_path, self.trash_path, self.info_path, 'cancel')
        with open(file_path, "w") as file:
            file.write("second file")
        self.t.delete(file_path, self.trash_path, self.info_path, 'cancel')

        self.assertFalse(os.path.exists(os.path.join(self.trash_path, "test_0001.txt")))

        self.assertTrue(os.path.exists(file_path))
        self.assertTrue(os.path.exists(os.path.join(self.trash_path, "test.txt")))
        
        with open(file_path, "r") as file:
            self.assertEqual(file.readline(), "second file")
        with open(os.path.join(self.trash_path, "test.txt"), "r") as file:
            self.assertEqual(file.readline(), "first file")

    def test_normal_working_restoring(self):
        file_path = os.path.join(self.path, "test.txt")
        with open(file_path, "w"):
            pass

        self.t.delete(file_path, self.trash_path, self.info_path)
        code = self.t.restore("test.txt", self.trash_path, self.info_path)

        self.assertEqual(code, 210)

        self.assertTrue(os.path.exists(file_path))
        self.assertFalse(os.path.exists(os.path.join(self.path, ".trash", "test.txt")))

    def test_nofile_restoring(self):
        code = self.t.restore("file_path", self.trash_path, self.info_path)

        self.assertEqual(code, 410)
        self.assertFalse(os.path.exists(os.path.join(self.path, "file_path")))

    def test_multiple_files_restoring(self):
        file_path1 = os.path.join(self.path, "test.txt")
        with open(file_path1, "w"):
            pass

        file_path2 = os.path.join(self.path, "some.txt")
        with open(file_path2, "w"):
            pass

        file_path3 = os.path.join(self.path, "python.txt")
        with open(file_path3, "w"):
            pass

        self.t.delete(file_path1, self.trash_path, self.info_path)
        self.t.delete(file_path2, self.trash_path, self.info_path)
        self.t.delete(file_path3, self.trash_path, self.info_path)
        self.t.restore("test.txt", self.trash_path, self.info_path) 
        self.t.restore("some.txt", self.trash_path, self.info_path)
        self.t.restore("python.txt", self.trash_path, self.info_path)

        self.assertTrue(os.path.exists(file_path1))
        self.assertTrue(os.path.exists(file_path2))
        self.assertTrue(os.path.exists(file_path3))

        self.assertFalse(os.path.exists(os.path.join(self.trash_path, "test.txt")))
        self.assertFalse(os.path.exists(os.path.join(self.trash_path, "some.txt")))
        self.assertFalse(os.path.exists(os.path.join(self.trash_path, "python.txt")))

    def test_conflict_gen_uniq_restoring(self):
        file_path = os.path.join(self.path, "teste.txt")
        with open(file_path, "w"):
            pass
        self.t.delete(file_path, self.trash_path, self.info_path, policy='replace')
        
        with open(file_path, "w"):
            pass

        code = self.t.restore("teste.txt", self.trash_path, self.info_path, policy='gen_uniq')
        self.assertEqual(code, 211)

        self.assertFalse(os.path.exists(os.path.join(self.trash_path, "teste.txt")))
        self.assertTrue(os.path.exists(os.path.join(self.path, "teste.txt")))
        self.assertTrue(os.path.exists(os.path.join(self.path, "teste__00001.txt")))

    def test_conflict_replace_restoring(self):
        file_path = os.path.join(self.path, "test.txt")
        with open(file_path, "w") as file:
            file.write("new file")
        self.t.delete(file_path, self.trash_path, self.info_path)
        with open(file_path, "w"):
            pass

        code = self.t.restore("test.txt", self.trash_path, self.info_path, policy='replace')
        self.assertEqual(code, 212)

        self.assertTrue(os.path.exists(file_path))
        self.assertTrue(os.path.exists(os.path.join(self.path, "test.txt")))
        self.assertFalse(os.path.exists(os.path.join(self.trash_path, "test__00001.txt")))

        with open(os.path.join(self.path, "test.txt"), "r") as file:
            self.assertEqual(file.readline(), "new file")

    def test_conflict_cancel_restoring(self):
        file_path = os.path.join(self.path, "test.txt")
        with open(file_path, "w") as file:
            file.write("first file")

        self.t.delete(file_path, self.trash_path, self.info_path)

        with open(file_path, "w") as file:
            file.write("second file")

        code = self.t.restore(file_path, self.trash_path, self.info_path, policy='cancel')
        self.assertEqual(code, 411)

        self.assertTrue(os.path.exists(file_path))

        self.assertTrue(os.path.exists(os.path.join(self.trash_path, "test.txt")))
        self.assertFalse(os.path.exists(os.path.join(self.trash_path, "test__00001.txt")))

        with open(file_path, "r") as file:
            self.assertEqual(file.readline(), "second file")
        with open(os.path.join(self.trash_path, "test.txt"), "r") as file:
            self.assertEqual(file.readline(), "first file")

    # def test_force_restoring(self):
    #     file_path1 = os.path.join(self.path, "test.txt")
    #     with open(file_path1, "w"):
    #         pass

    #     file_path2 = os.path.join(self.path, "permissions.txt")
    #     with open(file_path2, "w"):
    #         pass

    #     self.trash.delete(file_path1)
    #     self.trash.delete(file_path2)

    #     file_paths_to_restore = ["test.txt", "nofile.txt", "permissions.txt"]
    #     code = self.trash.restore(file_paths_to_restore, flags=RestoreFlags.force)
    #     self.assertEqual(code, 201)

    #     self.assertTrue(os.path.exists(file_path1))
    #     self.assertTrue(os.path.exists(file_path2))

    #     self.assertFalse(os.path.exists(os.path.join(self.path,  "test.txt")))
    #     self.assertFalse(os.path.exists(os.path.join(self.path, ".trash", "permissions.txt")))

    def tearDown(self):
        shutil.rmtree(self.path)


if __name__ == "__main__":
    unittest.main()
