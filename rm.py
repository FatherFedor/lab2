#! usr/bin/env python
import json
import logging

from config import initial_config
from parsermarg import parse_args
from trash import Trash


def write_config():
    PATH_CONFIG = '/home/light/PyLabs/Lab_0002/.config'
    setting = {
        'path directory of trash': '/home/light/PyLabs/Lab_0002/trash/files',
        'path directory of info files': '/home/light/PyLabs/Lab_0002/trash/info',
        'path directory of log': '/home/light/PyLabs/Lab_0002/trash/trash.log',
        'policy auto deleting': {'politic': 'time', 'value': '15'},
        'policy conflict deleting': 'gen_uniq',
        'policy conflict restore': 'gen_uniq',
        'max size': 1000
    }
    with open(PATH_CONFIG, 'w') as f:
        json.dump(setting, f, indent=4, separators=(',', ': '))
    return setting


def main():
    args = vars(parse_args())

    write_config()
    if args['config']:
        setting = initial_config(args['config'])
    else:
        setting = initial_config()
    logging.basicConfig(filename=setting['path directory of log'],
                        format='%(asctime)s %(message)s',
                        level=logging.DEBUG,
                        datefmt='%m/%d/%Y %I:%M:%S %p')
    logging.info('App start')
    # print args
    code = 0
    t = Trash(setting['policy conflict deleting'])
    t.auto_clean_trash(setting['path directory of trash'],
                       setting['path directory of info files'],
                       setting['policy auto deleting'])

    if args['command'] == 'show':
        code = t.show(setting['path directory of trash'])

    if args['command'] == 'delete':
        if args['regex']:
            t.regex_delete(args['regex'],
                           args['file'],
                           setting['path directory of trash'],
                           setting['path directory of info files'],
                           dry_run=args['dryrun'],
                           verbose=args['verbose'],
                           silent=args['silent'],
                           interactive=args['interactive'],
                           force=args['force'])
        else:
            code = t.delete(args['file'],
                            setting['path directory of trash'],
                            setting['path directory of info files'],
                            dry_run=args['dryrun'],
                            verbose=args['verbose'],
                            silent=args['silent'],
                            interactive=args['interactive'],
                            force=args['force'])

    if args['command'] == 'clean':
        code = t.clean_trash(setting['path directory of trash'])

    if args['command'] == 'restore':
        code = t.restore(args['file'],
                         setting['path directory of trash'],
                         setting['path directory of info files'],
                         setting['policy conflict restore'],
                         dry_run=args['dryrun'],
                         verbose=args['verbose'],
                         silent=args['silent'],
                         interactive=args['interactive'],
                         force=args['force'])
    logging.info('App end with code %s' % code)

if __name__ == '__main__':
    main()
